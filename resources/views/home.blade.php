@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            Make Demo Notifications!! <p>
                        </div>
                      </div>


                    <form method="POST" action="{{ route('notify') }}">
                        @csrf

                    <div class="form-group row">
                        <label for="notification" class="col-md-4 col-form-label text-md-right">Notification</label>

                        <div class="col-md-6">
                            <input id="notification" type="notification" class="form-control{{ $errors->has('notification') ? ' is-invalid' : '' }}" name="notification" value="{{ old('notification') }}" required>

                            @if ($errors->has('notification'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('notification') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="user_to" class="col-md-4 col-form-label text-md-right">To</label>

                        <div class="col-md-6">
                          <select id="user_to" type="text" class="form-control{{ $errors->has('user_to') ? ' is-invalid' : '' }}" name="user_to" value="{{ old('user_to') }}" required autofocus>
                                @foreach($users as $uu)
                                <option value="{{ $uu->id }}">{{ $uu->name }}</option>
                                @endforeach
                                <option value="user_all">All Users</option>
                                <option value="vendor_all">All Vendors</option>
                                <option value="customer_all">All Customer</option>
                          </select>

                            @if ($errors->has('user_to'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('user_to') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                          <button type="submit" class="btn btn-primary">
                              Notify
                          </button>
                        </div>
                    </div>


                  </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
