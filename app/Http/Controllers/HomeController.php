<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Users_info;
use App\Vendors_info;
use App\Customers_info;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // selects the notifications according to users type and user ID !!
        if(Auth::user()->user_type == '1'){
          $notification = Users_info::where('user_id','=',Auth::user()->id)->orWhere('user_id','=','user_all')->orderBy('id','desc')->get();
        }
        if(Auth::user()->user_type == '2'){
          $notification = Vendors_info::where('user_id','=',Auth::user()->id)->orWhere('user_id','=','vendor_all')->orderBy('id','desc')->get();
        }
        if(Auth::user()->user_type == '3'){
          $notification = Customers_info::where('user_id','=',Auth::user()->id)->orWhere('user_id','=','customer_all')->orderBy('id','desc')->get();
        }
        $users = User::orderBy('id')->get();
        return view('home')->with('notification',$notification)->with('users',$users);

    }


    // generating notification for testing purpose
    public function notify(Request $request){

      $user = User::where('id','=',$request->user_to)->first();
      if($user){
        if($user->user_type == '1'){
          $data = new Users_info;
        }

        if($user->user_type == '2'){
          $data = new Vendors_info;
        }

        if($user->user_type == '3'){
          $data = new Customers_info;
        }
      }
      else{
        if($request->user_to == 'user_all'){
          $data = new Users_info;
        }

        if($request->user_to == 'vendor_all'){
          $data = new Vendors_info;
        }

        if($request->user_to == 'customer_all'){
          $data = new Customers_info;
        }
      }


      $data->user_id = $request->user_to;
      $data->notification = $request->notification;
      $data->read_status = '0';

      $data->save();

      return redirect()->route('home');
      //echo  $request->user_to."<br>".$request->notification;

    }
}
